const api = {
  baseUrl: "https://api.themoviedb.org/3/",
  imageUrl: "https://image.tmdb.org/t/p/w500",
  movieAPIKey: process.env.REACT_APP_MOVIE_API_KEY,
};

export default api;
