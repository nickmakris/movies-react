import axios from "axios";
// import qs from "querystring";
import api from "../config";

export default async function makeRequest(path) {
  return await axios
    .get(api.baseUrl + path, {
      params: { api_key: api.movieAPIKey },
    })
    .then(({ status, data }) => {
      return status === 200 && data;
    })
    .catch((err) => console.error(err));
}
