import React from "react";

import api from "../../config";

function Poster({ className = "", alt = "", src = "" }) {
  return src && <img className={`img-fluid ${className}`} src={api.imageUrl + src} alt={alt} />;
}

export default Poster;
