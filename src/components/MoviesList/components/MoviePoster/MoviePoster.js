import React from "react";
import { Link } from "react-router-dom";

import Poster from "../../../Poster";

function MoviePoster({ id, title = "", poster = "" }) {
  return (
    <li>
      <Link to={`/movie/${id}`}>
        {poster && <Poster src={poster} />}
        <h3>{title}</h3>
      </Link>
    </li>
  );
}

export default MoviePoster;
