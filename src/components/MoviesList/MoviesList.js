import React, { Component } from "react";
import makeRequest from "../../api/makeRequest";

import Loading from "../Loading";
import MoviePoster from "./components/MoviePoster";
import { Row, Col, Navbar } from "react-bootstrap";
import Form from "react-bootstrap/Form";

import "bootstrap/dist/css/bootstrap.min.css";

class MoviesList extends Component {
  constructor() {
    super();

    this.handleInputChange = this.handleInputChange.bind(this);

    this.state = {
      loading: true,
      movieList: [],
      movieType: "popular",
    };
  }

  async handleInputChange(event) {
    const target = event.target;
    let value = target.value;

    if (value) {
      this.setState({ loading: true });

      const getNewList = await this.getMovieListing(value);

      this.setState({
        loading: false,
        movieList: getNewList || [],
        movieType: value,
      });
    }
  }

  getMovieListing = async (movieType) => {
    const getMovies = await makeRequest(`movie/${movieType}`);
    console.log(getMovies);
    return getMovies && getMovies.results;
  };

  async componentDidMount() {
    const movieList = await this.getMovieListing(this.state.movieType);
    this.setState({
      loading: false,
      movieList: movieList,
    });
  }

  render() {
    const { loading, movieList } = this.state;
    return (
      <div>
        <Navbar bg='light' expand='lg'>
          <Form inline>
            <Form.Group>
              <Form.Label className='my-1 mr-2' htmlFor='genreType'>
                Select List Type
              </Form.Label>
              <Form.Control as='select' className='my-1 mr-sm-2' id='genreType' custom onChange={this.handleInputChange}>
                <option value=''>Select</option>
                <option value='now_playing'>Now Playing</option>
                <option value='popular'>Popular</option>
                <option value='top_rated'>Top Rated</option>
                <option value='upcoming'>Upcoming</option>
              </Form.Control>
            </Form.Group>
          </Form>
        </Navbar>
        <Row className='movieListing'>
          <Col>
            <ul className='movies-list'>
              {loading && <Loading />}
              {movieList && movieList.map((movie) => <MoviePoster key={movie.id} id={movie.id} title={movie.title} poster={movie.poster_path} />)}
            </ul>
          </Col>
        </Row>
      </div>
    );
  }
}

export default MoviesList;
