import React, { useEffect, useState } from "react";
import { useLocation, Link } from "react-router-dom";

import makeRequest from "../../api/makeRequest";

import Row from "react-bootstrap/Row";

import Poster from "../Poster";
import Loading from "../Loading";
import MovieItem from "./components/MovieItem";

const movieData = {
  loading: true,
  data: [],
};

function MovieRecList(movieList) {
  return (
    <Row className='mb-4'>
      <h3 className='col-12'>Recommended Movies</h3>
      {movieList.map(({ id, title, poster_path }) => (
        <MovieItem key={id} title={title} id={id} poster={poster_path} />
      ))}
    </Row>
  );
}

function MovieSimList(movieList) {
  return (
    <Row className='mb-4'>
      <h3 className='col-12'>Similar Movies</h3>
      {movieList.map(({ id, title, poster_path }) => (
        <MovieItem key={id} title={title} id={id} poster={poster_path} />
      ))}
    </Row>
  );
}

function MovieDetail() {
  let location = useLocation();

  const getMovieDetails = async () => {
    const getMovie = await makeRequest(location.pathname);
    const getMovieData = getMovie;

    return setMovie({
      loading: false,
      data: getMovieData,
    });
  };

  const getRecommendedMovies = async () => {
    const getRecMovies = await makeRequest(`${location.pathname}/recommendations`);
    const getRecMovieData = getRecMovies && getRecMovies.results;

    return setRecMovies(getRecMovieData);
  };

  const getSimilarMovies = async () => {
    const getSimMovies = await makeRequest(`${location.pathname}/similar`);
    const getSimMovieData = getSimMovies && getSimMovies.results;

    return setSimMovies(getSimMovieData);
  };

  const [movie, setMovie] = useState(movieData);
  const [recMovies, setRecMovies] = useState([]);
  const [simMovies, setSimMovies] = useState([]);
  const { loading, data } = movie;

  useEffect(() => getMovieDetails(), []);
  useEffect(() => getRecommendedMovies(), []);
  useEffect(() => getSimilarMovies(), []);

  const { backdrop_path, genres, overview, poster_path, title } = data;
  return (
    <div>
      <div className='movie-split'>
        {!data.length && loading && <Loading />}
        {poster_path && <Poster className='backdrop' src={backdrop_path} alt='' />}
        <div className='block'>
          <Link to='/'>🔙</Link>
          <h1>{title}</h1>
          {overview && <p>{overview}</p>}
          <ul className='mb-4'>{genres && genres.length && genres.map(({ id, name }) => <li key={id}>{name}</li>)}</ul>
          {recMovies.length && MovieRecList(recMovies)}
          {simMovies.length && MovieSimList(simMovies)}
        </div>
        <div className='image'>{poster_path && <Poster src={poster_path} alt='' />}</div>
      </div>
    </div>
  );
}

export default MovieDetail;
