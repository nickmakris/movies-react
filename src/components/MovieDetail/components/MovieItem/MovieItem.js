import React from "react";
import { Link } from "react-router-dom";
import Col from "react-bootstrap/Col";

import Poster from "../../../Poster";

function MovieItem({ id, title = "", poster = "" }) {
  return (
    <Col xs={6} lg={4}>
      <Link to={`/movie/${id}`}>
        {poster && <Poster src={poster} />}
        <h4>{title}</h4>
      </Link>
    </Col>
  );
}

export default MovieItem;
